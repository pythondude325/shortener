# shortener

A basic URL shortener written in rust

## Systemd Service
To use the systemd service provided, replace the placeholders with appropriate
values then copy the service to the `/usr/local/lib/systemd/system/` directory
and activate.

## Todo
- Add support for requested link ID's
- Add a date created field to each shortcut so they can be sorted by date