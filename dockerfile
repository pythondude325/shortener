FROM rustlang/rust:nightly

WORKDIR /usr/src/shortener
COPY . .

RUN cargo install --path .

CMD ["shortener"]

EXPOSE 80
