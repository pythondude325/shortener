#[derive(Debug, serde::Serialize)]
pub struct Shortcut {
    pub id: i32,
    pub url: String,
    pub clicks: i32,
    pub owner: i64,
}

impl Shortcut {
    pub fn new(id: i32, url: impl Into<String>, clicks: i32, owner: i64) -> Shortcut {
        Shortcut {
            id,
            url: url.into(),
            clicks,
            owner,
        }
    }

    pub fn from_row(row: &postgres::Row) -> Shortcut {
        Shortcut::new(
            row.get(0),
            row.get::<_, String>(1),
            row.get(2),
            row.get(3),
        )
    }

    pub fn add_to_db(&self, conn: &mut postgres::Client) -> Result<(), postgres::Error> {
        conn.execute(
            "insert into shortcuts (id, url, clicks, owner) values ($1, $2, $3, $4)",
            &[
                &self.id,
                &self.url,
                &self.clicks,
                &self.owner,
            ],
        )?;

        Ok(())
    }
}
