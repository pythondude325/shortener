class Shortcut {
  constructor(shortcut_data){
    this.shortcut_id = shortcut_data.shortcut_id
    this.destination_url = shortcut_data.destination_url
    this.clicks = shortcut_data.clicks
  }

  make_shortcut_anchor(){
    let current = new URL(location.href)
    return `<a href="${current.origin}/${this.shortcut_id}">${current.host}/${this.shortcut_id}</a>`
  }
}

function refresh_shortcut_list() {
  fetch("/list").then( async (response) => {
    let data = await response.json()
  
    let shortcut_list = document.getElementById("shortcut-list")
    shortcut_list.innerHTML = ""

    for (let shortcut_response of data.shortcuts) {
      let s = new Shortcut(shortcut_response)

      let list_item = document.createElement("li")
      list_item.innerHTML = `${s.make_shortcut_anchor()} &rarr; <a href="${s.destination_url}">${s.destination_url}</a> (${s.clicks} clicks)`
      shortcut_list.appendChild(list_item)
    }

  })
}

function main(){
  let url_input = document.getElementById("url-input")
  let submit_button = document.getElementById("submit-button")
  let status_output = document.getElementById("status-output")

  async function submit() {
    let response = await fetch("/", {
      method: "POST",
      body: new Blob([JSON.stringify({
        url: url_input.value
      })], {type : "application/json"})
    })

    let shortcut = new Shortcut(await response.json())
    status_output.innerHTML = `Shortened URL: ${shortcut.make_shortcut_anchor()}`

    refresh_shortcut_list()
  }

  submit_button.addEventListener("click", submit)
  url_input.addEventListener("keydown", e => {
    if(e.key == "Enter") submit()
  })


  refresh_shortcut_list()

  setInterval(refresh_shortcut_list, 30000)
}

document.addEventListener("DOMContentLoaded", main)