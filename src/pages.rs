use rocket::http::{Cookie, Cookies};
use rocket::response::content;
use time::Duration;

#[get("/")]
pub fn main_page(mut cookies: Cookies) -> content::Html<&'static str> {
    if cookies.get("client_id").is_none() {
        let mut cookie = Cookie::new(
            "client_id",
            crockford::encode(u64::from(rand::random::<u32>())),
        );
        cookie.set_max_age(Duration::weeks(1_000_000));

        cookies.add(cookie);
    }

    content::Html(include_str!("static/index.html"))
}

#[get("/main.js")]
pub fn main_script() -> content::JavaScript<&'static str> {
    content::JavaScript(include_str!("static/main.js"))
}

#[get("/style.css")]
pub fn main_style() -> content::Css<&'static str> {
    content::Css(include_str!("static/style.css"))
}

#[get("/favicon.ico")]
pub fn favicon() -> content::Content<&'static [u8]> {
    content::Content(
        rocket::http::ContentType::GIF,
        include_bytes!("static/icon.gif"),
    )
}
